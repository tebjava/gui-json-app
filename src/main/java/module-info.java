/*
 * Należy pamiętać, że ten plik MUSI zawierać w wymaganych wszystkie użyte przez nas biblioteki i klasy javafx ORAZ
 * np. dostęp do klasy jsobject! Ponadto eksportujemy naszą głwną poczkę (inaczej program będzie zgłaszał dziwne błędy!)
 */

module org.openjfx.mainapp {
    requires javafx.controls;
	requires javafx.web;
	requires jdk.jsobject;
    exports org.openjfx.mainapp;
}