package org.openjfx.mainapp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class JSOperations {
	
	private String path;
	
	public JSOperations() {
		
	}
	
	public JSOperations(String path) {
		this.path=path;
	}
	
	public String getPath() {
		
		return path;
	}
	
	public String loadFileforJS(Object param) {
		//System.out.println("TUTAJ JESTEM!! "/* + param + " " + param2*/);
		return loadFileFromString(param);
	}
	
	public static String loadFileFromString(Object name) {
		//System.out.println("TUTAJ JESTEM!! " + name);
		return loadFile(new File((String) name));
	}
	
	public static String loadFile(File name) {
		try {
			//upewniamy się, że wczytane bajty zostaną zamienione na UTF-8!; inaczej będziemy mieli krzaki, nie litery
			return new String((new FileInputStream(name)).readAllBytes(),"UTF-8");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Object helloDemo() {
		System.out.println("KOMUNIKAT Z JAVA!");
		return "Witaj ze środka języka Java!";
	}
}
