package org.openjfx.mainapp;

import java.io.File;
import java.util.List;

import javafx.application.Application;
//obie poniższe biblioteki są WYMAGANE do połączenia z JavaScript!
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker.State;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import libs.JSONOperations;
import libs.JSONOperations.JSONObject;
//biblioteka WYMAGANA do przechwycenia skrypow JS na poziomie Java
import netscape.javascript.JSObject;


/**
 * JavaFX App
 */
public class App extends Application {

	WebEngine webEngine;
	
    @Override
    public void start(Stage stage) {
    	WebView wv = new WebView();
    	webEngine = wv.getEngine();
    	
    	webEngine.load(App.class.getResource("/html/window.html").toString());
    	//dopiero w poniższym kodzie możemy dokonać połączenia kodu Java z JavaScript (i na odwrot). Po pierwsze - mamy już załadowaną stronę głowną,
    	//w związku z tym pobieramy wątek ładujący ją, wyciągamy stany tegoż wątku, a z niego wybieramy dodanie nasłuchu. Musimy dodać obiekt typu ObservableValue 
    	//(to jest INTERFEJS) oraz stany - stary i nowy. Jak łatwo się domyślić - będziemy realizować stan nowej wartości (newValue).
    	webEngine.getLoadWorker().stateProperty().addListener((ObservableValue<? extends State> observable, State oldValue, State newValue) -> {
    		//jeżeli zmiana stanu zakończy się sukcesem (np. zostanie załadowana strona WWW) to wykona się kod z poniższego warunku
    		if (newValue==State.SUCCEEDED) {
    			//niestety JavaFX ma potężne problemy z wczytaniem skryptow w kodzie HTML - nie mają one żadnego efektu
    			//dlatego albo musimy dodać kod JS w pliku HTML albo wczytać go w ten oto sposob - tworzymy dowiązanie
    			//do pliku skryptu (pobieramy ścieżkę do niego poprzez lokalizację już pobranego pliku menu.html)
    			File f = new File((webEngine.getLocation().substring(0, webEngine.getLocation().lastIndexOf('/'))+"/script.js").split("///")[1]);
    			//mając już załadowany plik ładujemy jego zawartość poprzez executeScript (pozwala na wywoływanie dowolnego 
    			//kod JavaScript zapisanego w postaci ciągu znakowego)
    			webEngine.executeScript(String.valueOf(JSOperations.loadFile(f)));
    			//tutaj przykładowo zmieniamy nazwę naszego okna
    			stage.setTitle("Aplikacja zarządzania personelem");
    			//przechwytujemy dokument załadowany w naszej aplikacji; dzięki temu możemy np. dodać...
    			JSObject jsDocument = (JSObject) webEngine.getDocument();
    			//...zarejestrowany obiekt javaObj, ktory od teraz możemy wywołać z JavaScript (document.javaDOM)...
    			jsDocument.setMember("javaDOM", new JSOperations());
    			//...lub utworzyć dostęp do naszego okna i tym samym wywoływać kod Java bezpośrednio z głownego drzewka (skoro znajduje się
    			//w window to nie musimy pisać window.javaBOM tylko po prostu javaBOM
    			//Funkcja poniżej pozwala na wykonywanie elementow i metod JavaScript; w tym wypadku zwraca do 
    			//Java obiekt window (użyty do tego by przypisać do niego punkt zaczepienia obiektu z języka Java)
    			JSObject jsWindow = (JSObject) webEngine.executeScript("window");
    			jsWindow.setMember("javaBOM", new JSOperations((webEngine.getLocation().substring(0, webEngine.getLocation().lastIndexOf('/'))).split("///")[1])); //TO JEST NOWY OBIEKT, nie związany z poprzednim (poza nazwą)
    			//na koniec wywołujemy funkcję ze skryptu JavaScript ładującą nam menu oraz tekst startowy aplikacji
    			//bez tej linijki nasz program nie wyświetliłby ani przyciskow, ani tekstu!
    			webEngine.executeScript("loadData('main')");
    		}
    	});
    	
    	Scene scene = new Scene(wv);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
//        JSONOperations json = new JSONOperations();
//        json.parseFile("C:\\Users\\admin\\Documents\\przyklad.json");
//        //json.renderJSONFile("C:\\Users\\admin\\Documents\\render.json");
//        //Object tmp3=json.createJSONEntity("test", "nowy","drugi");
//        //json.getObject(name)
//        Object tmp = json.getObject(JSONObject.FIRSTARRAY);
//        json.addValue("test", 12);
//        tmp = json.addObject(tmp);
//        json.addValue(tmp, "imie", "Jakub");
//        json.addValue(tmp, "nazwisko", "Malicki");
//        json.addValue(tmp, "email", "jasnowidz@wp.pl");
//        Object tmp2 = json.getObjects("imie").get(0);
//        json.removeObject(json.getParentObject("imie"),tmp2);
//        json.getKeys();
//        json.renderJSONFile("C:\\Users\\admin\\Documents\\render.json");
//        return;
        //json.renderJSONContent();
    	launch(args);
    }

}