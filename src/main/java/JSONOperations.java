//package libs;
//
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.UnsupportedEncodingException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.stream.Collectors;
//
//public class JSONOperations {
//	
//	private FileInputStream fis;
//	
//	public enum JSONPos {
//		FIRST(0),
//		LAST(-1),
//		MIDDLE(0.5);
//		
//		private double d;
//		
//		private JSONPos(double d) { this.d = d; }
//		
//		public double get() { return this.d; }
//	}
//	
//	private class JSONEntity {
//		String name;
//		int index;
//		List<Object> value;
//	}
//	
//	List<JSONEntity> JSONElement;
//	
//	public List<Object>  getValue() {
//		return getValue(0);
//	}
//	
//	public List<Object>  getValue(JSONPos pos) {
//		if (pos.get()==0) {
//			return getValue(0);
//		}
//		if (pos.get()==0.5) {
//			return getValue(JSONElement.size()/2 - 1);
//		}
//		return getValue(JSONElement.size()-1);
//	}
//	
//	public List<Object> getValue(int pos) {
//		return JSONElement.get(pos).value;
//	}
//	
//	public List<Object> getValue(String name) {
//		return getValue(name, 0, 0);
//	}
//	
//	public List<Object> getValue(String name, int start) {
//		return getValue(name, start, 0);
//	}
//	
//	public List<Object> getValue(String name, int start, int end) {
//		List<Object> ret = null;
//		int found = 0;
//		
//		for (JSONEntity j:JSONElement.subList(start,JSONElement.size())) {
//			if (j.name==name) {
//				if (ret==null) ret = new ArrayList<Object>();
//				for (Object v:j.value) ret.add(v);
//				if (end<=found++)
//					break;
//			}
//		}
//		return ret;
//	}
//	
//	public List<Object> getAll(String name) {
//		return getValue(name, 0, JSONElement.size()-1);
//	}
//	
//	public void addElement() {
//		addElement("", 0);
//		
//	}
//	
//	public void addElement(String name, int index) {
//		if (JSONElement == null) {
//			JSONElement = new ArrayList<JSONEntity>();
//		}
//		for(JSONEntity jp: JSONElement) {
//			if (jp.index==index) return;
//		}
//		JSONEntity tmp = new JSONEntity();
//		tmp.index=index;
//		tmp.name=name;
//		JSONElement.add(tmp);
//	}
//	
//	public void addEntity( String name, Object value) {
//		
//	}
//	
//	public void addEntity(String name, List<Object> value) {
//		
//	}
//	
//	public void addEntity(String name, Object value, int element) {
//		
//	}
//	
//	public void addEntity(String name, Object value, String element) {
//		
//	}
//	
//	public void addEntity(String name, List<Object> value, int element) {
//		JSONEntity je = new JSONEntity();
//		je.value=value;
//		je.name=name;
//		if (element>-1) {
//			
//		}
//	}
//	
//	private Object searchEntity(String name) {
//		return searchEntity(name, JSONElement);
//	
//	}
//	
//	private Object searchEntity(String name, List<JSONEntity> search) {
//		Object ret=null;
//		boolean inside=false;
//		for (JSONEntity je: search) {
//			if (je.value.size()>1 && !inside) {
//				inside=true;
//				ret=searchEntity(name, je.value.stream().map(mapper -> (JSONEntity)mapper).collect(Collectors.toList()));
//			}
//
//			ret=je.value.stream().filter(mapper -> ((JSONEntity)mapper).name.equals(name)).findFirst().orElse(null);
//			
//			if (ret!=null)
//				break;
//		}
//		return ret;
//	}
//	
//	private List<JSONEntity> readJSONObject() {
//		return readJSONObject(false);
//		
//	}
//	
//	private String stringFromBytes(List<Byte> lb) {
//		return stringFromBytes(lb);
//	}
//	
//	@SuppressWarnings("unchecked")
//	private String stringFromBytes(Object o) {
//		if (o==null) return null;
//		ArrayList<Byte> b=null;
//		try {
//			b =(ArrayList<Byte>)o;
//		}
//		catch(Exception e) {
//			return null;
//		}
//		byte[] ret = new byte[b.size()];
//		for (int i=0;i<b.size();i++)
//			ret[i]=b.get(i);
//		//for(byte bt : b) 
//			
//		try {
//			return new String(ret,"UTF-16");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return null;
//	}
//	
//	private List<JSONEntity> readJSONObject(boolean hasKey) {
//		List<Byte> key, value;
//		key= new ArrayList<Byte>();
//		value= new ArrayList<Byte>();
//		List<JSONEntity> tmpListElement = new ArrayList<JSONOperations.JSONEntity>();
//		
//		try {
//			byte read = '\0';
//			while ((char)(read=(byte)fis.read())!='}') {
//				if (((char)read=='\'' || (char)read=='"') && key.equals("") && !hasKey)
//					for(;;) {
//						key.add(read);
//						read=(byte)fis.read();
//						if ((char)read=='\'' || (char)read=='"')
//							break;
//					}
//				if ((char)read=='{') {
//					Log.show("readJSONObject -> newObject " + key);
//					JSONEntity tmp= new JSONEntity();
//					
//					tmp.name= stringFromBytes(key).replace("\"","").replace("'", "");
//					tmp.value = readJSONObject().stream().map( m -> (Object)m).collect(Collectors.toList());
//					tmpListElement.add(tmp);
//					key.clear();
//					continue;
//				}
//				if ((char)read==',' && key.equals("") && value.equals("")) {
//					continue;
//				}
//				if ((char)read==','  && !key.isEmpty()) {
//					Log.show("readJSONObject -> element " + key);
//					JSONEntity tmp= new JSONEntity();
//					
//					tmp.name= stringFromBytes(key).replace("\"","").replace("'", "");
//					tmp.value = new ArrayList<Object>();
//					tmp.value.add(new String(value.replace("\"","").replace("[", "").replace("'", "").substring(value.indexOf(':')).getBytes(), "UTF-8"));
//					
//					tmpListElement.add(tmp);
//					key.clear();
//					value.clear();;
//					continue;
//				}
//				if ((char)read=='[') {
//					Log.show("readJSONObject -> newArray " + key);
//					JSONEntity tmp= new JSONEntity();
//					tmp.value = new ArrayList<Object>();
//					for (JSONEntity t : readJSONArray()) {
//						tmp.value.add(t);
//					}
//					
//					tmp.name=stringFromBytes(key).replace("\"","").replace("'", "");
//					tmpListElement.add(tmp);
//					key.clear();
//					value.clear();
//					continue;
//				}
//				if (key.size()>0 || hasKey)
//					value.add(read);
//			}
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		if (key.size()>0) {
//			JSONEntity tmp= new JSONEntity();
//			try {
//				tmp.name= new String(key.replace("\"","").replace("'", "").getBytes(), "UTF-8");
//			} catch (UnsupportedEncodingException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			tmp.value = new ArrayList<Object>();
//			try {
//				tmp.value.add(new String(value.replace("\"","").replace("[", "").replace("'", "").substring(value.indexOf(':')).getBytes(), "UTF-8"));
//			} catch (UnsupportedEncodingException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			if (JSONElement==null)
//				JSONElement = new ArrayList<JSONOperations.JSONEntity>();
//			tmpListElement.add(tmp);
//		}
//		
//		return tmpListElement;
//	}
//	
//	private List<JSONEntity> readJSONArray() {
//		String vals="";
//		//String key="";
//		List<JSONEntity> tmpListElement = new ArrayList<JSONOperations.JSONEntity>();
//		JSONEntity tmp=new JSONEntity();
//		tmp.value=new ArrayList<Object>();
//		tmp.name="";
//		try {
//			byte read = '\0';
//			while ((char)(read=(byte)fis.read())!=']') {
//				if ((char)read=='{') {
//					Log.show("readJSONArray -> newObject " + vals);
//					tmp.value.add(readJSONObject());
//					vals="";
//					continue;
//				}
//				if ((char)read==':') {
//					Log.show("readJSONArray -> newVal " + vals);
//					tmp.name=vals.replace("\"","").replace("'", "");
//					vals="";
//					continue;
//				}
//				if ((char)read==',' && !vals.isEmpty()) {
//					Log.show("readJSONArray -> newElement " + vals);
//					tmp.value.add(vals.replace("\"","").replace("'", ""));
//					vals="";
//					continue;
//				}
//				
//				vals+=(char)read;
//			}
//			if (vals.length()>0 && !vals.matches("^[\r\n\t]{1,}")) 
//				tmp.value.add(vals.replace("\"","").replace("'", "").replace("[", ""));
//						
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		//System.out.println("WYNIK TABLICA: " + vals);
//		if (tmpListElement.isEmpty())
//			tmpListElement.add(tmp);
//		return tmpListElement;
//	}
//	
//	private List<JSONEntity> checkIsJSONEntityArray(Object o) {
//		List<JSONEntity> r = null;// = new ArrayList<JSONOperations.JSONEntity>();
//		try {
//			JSONEntity t = (JSONEntity)o;
//			r = new ArrayList<JSONEntity>();
//			r.add(t);
//		}
//		catch(Exception e) {
//			
//		}
//		try {
//			@SuppressWarnings("unchecked")
//			List<JSONEntity> t = (ArrayList<JSONEntity>)o;
//			r=t;
//		}
//		catch(Exception e) {
//			
//		}
//		return r;
//	}
//	
//	public boolean renderJSONContent() {
//		return renderJSONContent(JSONElement);
//	}
//	
//	public boolean renderJSONContent(List<JSONEntity> list) {
//		if (list==null) return false;
//		for (JSONEntity jsonEntity : list) {
//			System.out.println(jsonEntity.name + " : ");
//			for (Object o : jsonEntity.value) {
//				//if (o.getClass()==JSONEntity.class)
//				if (!renderJSONContent(checkIsJSONEntityArray(o)))
//					System.out.print("\t\t" + o);
//				
//			}//System.out.println();
//			System.out.println();
//		}
//		return true;
//	}
//	
//	public void parseFile(String f) {
//		try {
//			if (JSONElement==null)
//				JSONElement = new ArrayList<JSONOperations.JSONEntity>();
//			fis = new FileInputStream(f);
//			byte read = (byte) fis.read();
//			if ((char)read!='[' && (char)read!='{') {
//				System.err.println("To nie jest plik JSON! Kończę pracę");
//				return;
//			}
//			//boolean endFile=false;
//			for(;;) {
//				if ((int)(read=(byte)fis.read())==-1) 
//					break;
//				if ((char)read=='{') {
//					Log.show("parseFile " + (char)read);
//					//log((char)read); 
//					JSONElement.addAll(readJSONObject());
//				}
//				if ((char)read=='[') {
//					Log.show("parseFile " + (char)read);
//					JSONElement.addAll(readJSONArray());
//				}
//			}
// 			
////			byte read[] = {'\0', '\0'};
////			for (;;) {
////				
////				if ((read=fis.readNBytes(2)).length==0) {
////					break;
////				}
////				Character
////				System.out.println((char)read);
////			}
//			//while((read=fis.readNBytes(2))[1]!=Character.MAX_CODE_POINT) {
//				
//			//}
//			//read[1] = '\0';
//			//System.out.println(new String(fis.readAllBytes()));
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		try {
//			fis.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
//	
//	
//} 
