var kluczoweStrony = ['main', 'osoby', 'place']

/*
 * Poniższa metoda działałaby w przypadku tworzenia domyślnej strony WWW, dostępnej z serwera WWW. Jednak w przypadku
 * lokalnego ładowania funkcja wywoła błąd (tego typu rozwiązanie, bez powiązania z serwerem mogłoby ułatwić atak XSS),
 * w związku z tym musimy poszukać innego sposobu ładowania zawartości naszej strony...
 */

function loadData(file) {
	var xml = new XMLHttpRequest()
	xml.onstatuschange=function(e) {
							if (xml.readyState===4) {
								if (xml.status===200 || xml.status===0) {
									loadMenu(xml.responseText)
									
									return
								}
								
							}
						}
	xml.open("POST", file,true)
	xml.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
	xml.send()
}

/*
 * Wykorzystamy poniższą funkcję do załadowania naszej strony-zawartości. Funkcja ta, po nazwie pliku, załaduje poprzez nasz program
 * Java, zawartości wskazanych plików i wyświetli je nam na stronie WWW. Funkcja wywołuje metodę dostępną w naszej aplikacji Java.
 */

function loadData(file) {
	//document.querySelector("main").innerHTML=javaBOM.getPath()+"/menus/"+file+".html"
	//loadMenu(javaBOM.helloDemo())
	if (kluczoweStrony.indexOf(file)>-1)
		loadMenu(javaBOM.loadFileforJS(javaBOM.getPath()+"/menus/"+file+".html"),file)
	loadPage(javaBOM.loadFileforJS(javaBOM.getPath()+"/pages/"+file+".html"))
	//loadMenu(javaBOM.loadFileFromString(javaBOM.getPath()+"/menus/"+file+".html"))
	//file=javaBOM.loadFileFromString(javaBOM.getPath()+"/menus/"+file+".html");
	//loadMenu(javaBOM.loadFile(javaBOM.getPath()+"/menus/"+file+".html"))
	//loadPage(javaBOM.getPath()+"/menus/"+file+".html")
}

function loadMenu(v,n) {
	document.querySelector("nav").innerHTML=v
	if (n!==undefined)
		addMenuButtonEvents(n)
}

function loadPage(v) {
	document.querySelector("main").innerHTML=v
}

function addMenuButtonEvents(name) {
	document.querySelectorAll(".button").forEach(
			(v) => { v.addEventListener("click", function(event) {loadData(((e) => {return e.dataset.page || e.parentNode.dataset.page})(event.target))})})
//	switch (name) {
//		case 'osoby': osobyEvents(); break
//		case 'main': startEvents(); break
//	}
}

//function osobyEvents() {
//	document.querySelectorAll(".button").forEach((v) => {
//		
////		if (v.dataset.page==='menu') {
////			v.addEventListener('click', function (e) {
////				
////			})
////		}
//	})
//}
//
//function startEvents() {
//	document.querySelectorAll(".button").forEach(
//			(v) => { v.addEventListener("click", function(event) {loadData(((e) => {return e.dataset.page || e.parentNode.dataset.page})(event.target))})})
//}




