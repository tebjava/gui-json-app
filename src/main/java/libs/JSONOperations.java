package libs;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class JSONOperations {
	
	private FileInputStream fis;
	private FileOutputStream fos;
	
	public enum JSONObject {
		FIRSTARRAY(-1),
		FIRSTOBJECT(-2),
		LASTARRAY(-3),
		LASTOBJECT(-4),
		ARRAY(-100),
		OBJECT(-101);
		
		private int d;
		
		private JSONObject(int d) { this.d = d; }
		
		public int get() { return this.d; }
	}
	
	private class JSONEntity {
		String name;
		//int index;
		List<Object> value;
		
		public Object element(int i) {
			return (i>0) ? value.get(i): null;
		}
		
		public JSONEntity subElement(int i) {
			try {
				return (i>0) ? (JSONEntity)value.get(i): null;
			}
			catch (Exception e) {
				
			}
			return null;
		}
	}
	
	private List<JSONEntity> JSONElement;
	
	public Object createJSONEntity(String key, Object... val) {
		List<Object> t = new ArrayList<Object>();
		t.add(val);
		return createJSONEntity(key, t);
	}
	
	public Object createJSONEntity(String key, List<Object> vals) {
		JSONEntity j = new JSONEntity();
		j.name=key;
		j.value=vals;
		return j;
	}
	
	public Object createJSONEntity(Object o) {
		try {
			//JSONEntity t = (JSONEntity)o;
			return (JSONEntity)o;
		}
		catch (Exception e) {			
		}
		return null;
	}
	
	public Object getObject(JSONObject jo) {
		JSONObject type=jo;
		if (jo==JSONObject.ARRAY) 
			jo=JSONObject.FIRSTARRAY;
		else if (jo==JSONObject.OBJECT) 
			jo=JSONObject.FIRSTOBJECT;
		else if (jo==JSONObject.FIRSTARRAY || jo==JSONObject.LASTARRAY) 
			type=JSONObject.ARRAY;
		else if (jo==JSONObject.FIRSTOBJECT || jo==JSONObject.LASTOBJECT) 
			type=JSONObject.OBJECT;
		return getObject(jo, type);
	}
	
	public Object getObject(JSONObject jo, JSONObject type) {
		return getObject(jo.get(),type.get());
	}
	
	public Object getObject(int i, String type) {
		return getObject(i, (type.equalsIgnoreCase("ARRAY") ? JSONObject.ARRAY.get() : JSONObject.OBJECT.get()));
	}
	
	public Object getObject(int i) {
		return getObject(i, JSONObject.ARRAY.get());
	}
	
	public Object getObject(int i, int type) {
		if (type==-100)
			return getArrayObject(i);
		return getObjectObject(i);
	}
	
	private Object getArrayObject(int i) {
		
		return getArrayObject(i, JSONElement);
	}
	
	@SuppressWarnings("unchecked")
	private Object getArrayObject(int i, Object l) {
		try {
			//System.out.println(l.getClass() +"\n" + (new ArrayList<JSONEntity>()).getClass());
			if (l.getClass() != (new ArrayList<JSONEntity>()).getClass()) {
				if (((JSONEntity)l).name.equals("")) 
					return l;
			}
			if (i==0) throw new Exception();
			for (Object ojson : (List<Object>)l) {
				JSONEntity json = (JSONEntity)ojson;
				if (i==-1 || i>0) {
					if (json.name.equals("")) return json;
					return getArrayObject((i==-1)?i:--i, json.value);
				}
				
			}
		}
		catch (Exception e) {
			
		}
		return null;
	}
	
	private Object getObjectObject(int i) {
		return getObjectObject(i, JSONElement);
	}
	
	@SuppressWarnings("unchecked")
	private Object getObjectObject(int i, Object l) {
		try {
			if (l.getClass() != ArrayList.class) {
				if (!((JSONEntity)l).name.equals("")) 
					return l;
			}
			if (i==0) throw new Exception();
			for (Object ojson : (List<Object>)l) {
				JSONEntity json = (JSONEntity)ojson;
				if (i==-2 || i>0) {
					return getArrayObject((i==-2)?i:--i, json.value);
				}
				
			}
		}
		catch (Exception e) {
			
		}
		return null;
	}
	
	public Object getParentObject(Object name) {
		Object tmp = null;
		if ((tmp=findValue(JSONElement, name, null, true, JSONElement))==null) return null;
		return tmp;
	}
	
	public Object getObject(Object name) {
		Object tmp = null;
		if ((tmp=findValue(JSONElement, name))==null) return null;
		return tmp;
	}
	
	public List<Object> getObjects(Object name) {
		List<Object> ret = new ArrayList<Object>();
		Object tmp = null;
		while((tmp=findValue(JSONElement, name, ret, true))!=null) {
			ret.add(tmp);
		}
		return ret;
	}
	
	public Object getValue(Object name) {
		return ((JSONEntity)getObject(name)).value.get(0);
	}
	
	public List<Object> getValues(Object name) {
		List<Object> ret = new ArrayList<Object>();
		for (Object o : getObjects(name)) 
			ret.add(((JSONEntity)o).value.get(0));
		return ret;
	}
	
	public List<String> getKeys() {
		List<String> ret = findNames(JSONElement);
//		for (Object o : getAllObjects()) {
//			if (ret.indexOf(o)!=-1) ret.add(String.valueOf(o));
//		}
		return ret;
	}
	
	private Object findValue(List<JSONEntity> j, Object o) {
		return findValue(j, o, null, false);
	}
	
//	private List<Object> getAllObjects() {
//		List<Object> ret = new ArrayList<Object>();
//		Object tmp = null;
//		for (JSONEntity jsonEntity : JSONElement) {
//			if (!jsonEntity.name.isEmpty())
//			if (jsonEntity.value.getClass() == ArrayList.class) {
//				System.out.println("TO JEST LISTA!");
//			}
//		}
//		return ret;
//	}
	
	private List<String> findNames(Object j) {
		return findNames(j, new ArrayList<String>());
	}
	
	@SuppressWarnings({ "unchecked", "unlikely-arg-type" })
	private List<String> findNames(Object j, List<String> ret) {
		if (j.getClass() != ArrayList.class && ret.indexOf(((JSONEntity)j).name)==-1)
			ret.add(((JSONEntity)j).name);
		else
			for (Object jsonEntity : (ArrayList<Object>)j) {
				try {
					JSONEntity tmp = (JSONEntity)jsonEntity;
					if (!tmp.name.isEmpty() && ret.indexOf(tmp.name)==-1) ret.add(tmp.name);
					if (tmp.value.getClass() == ArrayList.class) {
						findNames(tmp.value, ret);
					}
				}
				catch (Exception e) {
					if (jsonEntity.getClass() == ArrayList.class)
						findNames(jsonEntity, ret);
				}
			}
		return ret;
	}
	
	private Object findObject(Object o) {
		return findObject(JSONElement, o);
	}
	
	@SuppressWarnings("unchecked")
	private Object findObject(Object j, Object o) {
		Object ret=null;
		if (j.getClass() == ArrayList.class)
			for (Object json : (ArrayList<Object>)j) {
				if (ret!=null) return ret;
				if (json.equals(o)) return json; 
				try {
					ret = findObject(((JSONEntity)json).value, o);
				}
				catch (Exception e) {
					
				}
			}
			
		return ret;
	}
	
	private Object findValue(List<JSONEntity> j, Object o, List<Object> prev, boolean whole) {
		return findValue(j, o, prev, whole, null);
	}
	
	private Object findValue(List<JSONEntity> j, Object o, List<Object> prev, boolean whole, Object parent) {
		if (j==null) return null;
		//boolean findString=false;
		if (o != null && o.getClass() == String.class) {
			return findStrValue(j, String.valueOf(o), prev,whole,parent);
			//findString=true;
		}
		else {
			try {
				return findJSONValue(j, (JSONEntity)o, prev,whole,parent);
			}
			catch (Exception e) {
				
			}
		}
//		for (JSONEntity jsonEntity : j) {
//			if (findString) {				
//				if (jsonEntity.name.equals(String.valueOf(o))==true) {
//					if (prev.indexOf(jsonEntity)!=-1) continue;
//					else return jsonEntity;
//				}
//			}
//			for (Object ob : jsonEntity.value) {
//				Object r;
//				if ((r=findValue(checkIsJSONEntityArray(ob), o,prev))==null) {
//					if (ob.equals(o))
//						return ob;
//				}
//				else {
//					if (((JSONEntity)r).name.equals(o)) 
//						return (prev.indexOf(r)!=-1) ? null : r;
//					try {
//						if (findString) 
//							if (((JSONEntity)ob).name.equals(o)) return ob;
//					} catch(Exception e) {
//						
//					}
//				}
//			}
//		}
		return null;
	}
	
	private Object findStrValue(List<JSONEntity> j, String o, List<Object> prev, boolean whole, Object parent) {
		if (j==null) return null;
		for (JSONEntity jsonEntity : j) {						
			if (jsonEntity.name.equals(o)==true) {
				Object r = (parent!=null) ?parent:j;
				if (prev!=null) {					
					if (prev.indexOf(jsonEntity)!=-1) continue;
//					else {						
//						return (whole) ? r : jsonEntity;
//					}
				}
				return (whole) ? r : jsonEntity;
//				else return jsonEntity;
			}			
			for (Object ob : jsonEntity.value) {
				Object r;
				if ((r=findStrValue(checkIsJSONEntityArray(ob), o,prev,whole,(parent!=null)?jsonEntity:null))==null) {
					if (ob.equals(o))
						return (whole) ? jsonEntity : ob;
				}
				else {
					if (!whole) {
						if (((JSONEntity)r).name.equals(o)) 
							return (prev!=null && prev.indexOf(r)!=-1) ? null : r;
						try {						
							if (((JSONEntity)ob).name.equals(o)) return ob;
						} catch(Exception e) {
							
						}
					}
					else {
						return (prev!=null && prev.indexOf(r)!=-1) ? null : r;
					}
				}
			}
		}
		return null;
	}
	
	private Object findJSONValue(List<JSONEntity> j, JSONEntity o, List<Object> prev, boolean whole, Object parent) {
		if (j==null) return null;
		for (JSONEntity jsonEntity : j) {						
			if (jsonEntity.equals(o)==true) {
				if (prev!=null) {
					if (prev.indexOf(jsonEntity)!=-1) continue;
					else return jsonEntity;
				}
				else return jsonEntity;
			}
			
			for (Object ob : jsonEntity.value) {
				Object r;
				if ((r=findJSONValue(checkIsJSONEntityArray(ob), o,prev,whole,(parent!=null)?jsonEntity:null))==null) {
					if (ob.equals(o))
						return (whole) ? jsonEntity : ob;
				}
				else {
					if (((JSONEntity)r).equals(o)) 
						return (prev!=null && prev.indexOf(r)!=-1) ? null : r;
					try {						
						if (((JSONEntity)ob).equals(o)) return ob;
					} catch(Exception e) {
						
					}
				}
			}
		}
		return null;
	}
	
	public Object removeValue(String key, Object value) {
		return removeValue(JSONElement.get(0),key, value);
		//JSONElement.get(0).element(12);
	}
	
	public Object removeObject(Object o) {
		return removeValue(JSONElement.get(0), null, o);
	}
	
	public Object removeObject(Object parent, Object o) {
		return removeValue(parent, null, o);
	}
	
	@SuppressWarnings("unchecked")
	public Object removeValue(Object parent, String key, Object value) {
		Object ret = null;
		Object j; 

		try {
			j=value;
		}
		catch (Exception e) {
			j = new JSONEntity();
			((JSONEntity)j).name=key;
			((JSONEntity)j).value = new ArrayList<Object>();
			((JSONEntity)j).value.add(value);
		}
		ret = (findObject(parent));
		try {			
			((JSONEntity)ret).value.remove(j);// ((JSONEntity)ret).value.indexOf(j);
		}
		catch (Exception e) {
			((ArrayList<Object>)ret).remove(j);
		}
		return ret;
	}
	
	public Object addValue(String key, Object value) {
		return addValue(JSONElement.get(0),key, value);
		//JSONElement.get(0).element(12);
	}
	
	@SuppressWarnings("unchecked")
	public Object addValue(Object parent, String key, Object value) {
		Object ret = null;
		JSONEntity j = new JSONEntity();
		j.name=key;
		j.value = new ArrayList<Object>();
		j.value.add(value);
		ret = (findObject(parent));
		try {			
			((JSONEntity)ret).value.add(j);
		}
		catch (Exception e) {
			((ArrayList<Object>)ret).add(j);
		}
		return ret;
	}
	
	public Object addObject(Object parent) {
		return addObject(parent, "");
	}
	
	public Object addObject(Object parent, String key) {
		JSONEntity ret = null;
		Object addingObj = null;
		try {
			JSONEntity tmp = new JSONEntity();
			tmp.name=key;
			tmp.value = new ArrayList<Object>();
			addingObj = (key.equals("")) ? (new ArrayList<Object>()) : tmp;
			ret = ((JSONEntity)findObject(parent));
			ret.value.add(addingObj);
			ret = tmp;
			//ret = (key.equals("")) ? (new ArrayList<Object>()) : tmp;
		}
		catch (Exception e) {
			
		}
		return (key.equals("")) ? addingObj : ret;
	}
	
	
//	
//	public List<Object>  getValue() {
//		return getValue(0);
//	}
//	
//	public List<Object>  getValue(JSONPos pos) {
//		if (pos.get()==0) {
//			return getValue(0);
//		}
//		if (pos.get()==0.5) {
//			return getValue(JSONElement.size()/2 - 1);
//		}
//		return getValue(JSONElement.size()-1);
//	}
//	
//	public List<Object> getValue(int pos) {
//		return JSONElement.get(pos).value;
//	}
//	
//	public List<Object> getValue(String name) {
//		return getValue(name, 0, 0);
//	}
//	
//	public List<Object> getValue(String name, int start) {
//		return getValue(name, start, 0);
//	}
//	
//	public List<Object> getValue(String name, int start, int end) {
//		List<Object> ret = null;
//		int found = 0;
//		
//		for (JSONEntity j:JSONElement.subList(start,JSONElement.size())) {
//			if (j.name==name) {
//				if (ret==null) ret = new ArrayList<Object>();
//				for (Object v:j.value) ret.add(v);
//				if (end<=found++)
//					break;
//			}
//		}
//		return ret;
//	}
//	
//	public List<Object> getAll(String name) {
//		return getValue(name, 0, JSONElement.size()-1);
//	}
//	
//	public void addElement() {
//		addElement("", 0);
//		
//	}
	
	public void addElement(String name, int index) {
//		if (JSONElement == null) {
//			JSONElement = new ArrayList<JSONEntity>();
//		}
//		for(JSONEntity jp: JSONElement) {
//			if (jp.index==index) return;
//		}
//		JSONEntity tmp = new JSONEntity();
//		tmp.index=index;
//		tmp.name=name;
//		JSONElement.add(tmp);
	}
	
	public void addEntity( String name, Object value) {
		
	}
	
	public void addEntity(String name, List<Object> value) {
		
	}
	
	public void addEntity(String name, Object value, int element) {
		
	}
	
	public void addEntity(String name, Object value, String element) {
		
	}
	
	public void addEntity(String name, List<Object> value, int element) {
		JSONEntity je = new JSONEntity();
		je.value=value;
		je.name=name;
		if (element>-1) {
			
		}
	}
	
	private Object searchEntity(String name) {
		return searchEntity(name, JSONElement);	
	}
	
	private Object searchEntity(String name, List<JSONEntity> search) {
		Object ret=null;
		boolean inside=false;
		for (JSONEntity je: search) {
			if (je.value.size()>1 && !inside) {
				inside=true;
				ret=searchEntity(name, je.value.stream().map(mapper -> (JSONEntity)mapper).collect(Collectors.toList()));
			}
			ret=je.value.stream().filter(mapper -> ((JSONEntity)mapper).name.equals(name)).findFirst().orElse(null);			
			if (ret!=null)
				break;
		}
		return ret;
	}
	
	private String stringFromBytes(List<Byte> lb, char start) {
		String ret = stringFromBytes(lb);
		ret=ret.substring(ret.indexOf(start)+1);
		return ret;
	}
	
	private String stringFromBytes(List<Byte> lb, char start, char end) {
		String ret = stringFromBytes(lb);
		return ret.substring(ret.indexOf(start), ret.indexOf(end));
	}
	
	private String stringFromBytes(List<Byte> lb) {
		byte[] ret = new byte[lb.size()];
		for (int i=0;i<lb.size();i++)
			ret[i]=lb.get(i);			
		try {
			return new String(ret,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private List<JSONEntity> readJSONObject() {
		return readJSONObject(false);		
	}	
	
	private List<JSONEntity> readJSONObject(boolean hasKey) {
		List<Byte> key, value;
		key= new ArrayList<Byte>();
		value= new ArrayList<Byte>();
		List<JSONEntity> tmpListElement = new ArrayList<JSONOperations.JSONEntity>();		
		try {
			byte read = '\0';
			while ((char)(read=(byte)fis.read())!='}') {
				if (((char)read=='\'' || (char)read=='"') && key.isEmpty() && !hasKey)
					for(;;) {
						key.add(read);
						read=(byte)fis.read();
						if ((char)read=='\'' || (char)read=='"')
							break;
					}
				if ((char)read=='{') {
					//Log.show("readJSONObject -> newObject " + key);
					JSONEntity tmp= new JSONEntity();					
					tmp.name= stringFromBytes(key).replace("\"","").replace("'", "");
					tmp.value = readJSONObject().stream().map( m -> (Object)m).collect(Collectors.toList());
					tmpListElement.add(tmp);
					key.clear();
					continue;
				}
				if ((char)read==',' && key.isEmpty() && value.isEmpty()) {
					continue;
				}
				if ((char)read==','  && !key.isEmpty()) {
					//Log.show("readJSONObject -> element " + key);
					JSONEntity tmp= new JSONEntity();					
					tmp.name= stringFromBytes(key).replace("\"","").replace("'", "");
					tmp.value = new ArrayList<Object>();
					tmp.value.add(stringFromBytes(value,':').replace("\"","").replace("[", "").replace("'", ""));					
					tmpListElement.add(tmp);
					key.clear();
					value.clear();;
					continue;
				}
				if ((char)read=='[') {
					//Log.show("readJSONObject -> newArray " + key);
					JSONEntity tmp= new JSONEntity();
					tmp.value = new ArrayList<Object>();
					for (JSONEntity t : readJSONArray()) {
						tmp.value.add(t);
					}					
					tmp.name=stringFromBytes(key).replace("\"","").replace("'", "");
					tmpListElement.add(tmp);
					key.clear();
					value.clear();
					continue;
				}
				if (key.size()>0 || hasKey)
					value.add(read);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (key.size()>0) {
			JSONEntity tmp= new JSONEntity();
			tmp.name= stringFromBytes(key).replace("\"","").replace("'", "");
			tmp.value = new ArrayList<Object>();
			tmp.value.add(stringFromBytes(value,':').replace("\"","").replace("[", "").replace("'", ""));
			if (JSONElement==null)
				JSONElement = new ArrayList<JSONOperations.JSONEntity>();
			tmpListElement.add(tmp);
		}
		
		return tmpListElement;
	}
	
	private List<JSONEntity> readJSONArray() {
		List<Byte> vals = new ArrayList<Byte>();
		List<JSONEntity> tmpListElement = new ArrayList<JSONOperations.JSONEntity>();
		JSONEntity tmp=new JSONEntity();
		tmp.value=new ArrayList<Object>();
		tmp.name="";
		try {
			byte read = '\0';
			while ((char)(read=(byte)fis.read())!=']') {
				if ((char)read=='{') {
					//Log.show("readJSONArray -> newObject " + vals);
					tmp.value.add(readJSONObject());
					vals.clear();
					continue;
				}
				if ((char)read==':') {
					//Log.show("readJSONArray -> newVal " + vals);
					tmp.name=stringFromBytes(vals).replace("\"","").replace("'", "");
					vals.clear();
					continue;
				}
				if ((char)read==',' && !vals.isEmpty()) {
					//Log.show("readJSONArray -> newElement " + vals);
					tmp.value.add(stringFromBytes(vals).replace("\"","").replace("'", ""));
					vals.clear();
					continue;
				}
				
				vals.add(read);
			}
			String last=stringFromBytes(vals);
			if (!vals.isEmpty() && !last.matches("^[\r\n\t]{1,}")) 
				tmp.value.add(last.replace("\"","").replace("'", "").replace("[", ""));
						
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (tmpListElement.isEmpty())
			tmpListElement.add(tmp);
		return tmpListElement;
	}
	
	private List<JSONEntity> checkIsJSONEntityArray(Object o) {
		List<JSONEntity> r = null;
		try {
			JSONEntity t = (JSONEntity)o;
			r = new ArrayList<JSONEntity>();
			r.add(t);
		}
		catch(Exception e) {
			
		}
		try {
			@SuppressWarnings("unchecked")
			List<JSONEntity> t = (ArrayList<JSONEntity>)o;
			r=t;
		}
		catch(Exception e) {
			
		}
		return r;
	}
	
	public boolean renderJSONContent() {
		return renderJSONContent(JSONElement);
	}
	
	public boolean renderJSONContent(List<JSONEntity> list) {
		if (list==null) return false;
		for (JSONEntity jsonEntity : list) {
			System.out.println(jsonEntity.name + " : ");
			for (Object o : jsonEntity.value) {
				if (!renderJSONContent(checkIsJSONEntityArray(o)))
					System.out.print("\t\t" + o);
				
			}
			System.out.println();
		}
		return true;
	}
	
	public void renderJSONFile(String f) {
		if (JSONElement==null) return;
		try {
			fos = new FileOutputStream(f);
			String ret = "[{"+ createJSONString(JSONElement);
			ret=ret.substring(0,ret.length()-1) + "}]";
			fos.write(ret.getBytes());
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private String createJSONString(List<JSONEntity> list) {
		String ret = "";
		String fillName="";
		String vals="";
		if (list==null) return null;
		boolean isArray=false;
		boolean noBrackets=false;
		for (JSONEntity jsonEntity : list) {
			if (!jsonEntity.name.isEmpty())
				fillName+=((!fillName.isEmpty()) ? "," : "") + "\""+jsonEntity.name+"\"";
			else
				fillName="";
			if (jsonEntity.value.size()==1)
				noBrackets=true;
			for (Object o : jsonEntity.value) {
				String tmp = createJSONString(checkIsJSONEntityArray(o));
				if (tmp!=null) {
					vals+=tmp;
				}
				else
					vals+="\""+o+"\",";
			}
			if (!fillName.isEmpty())
				ret+=vals.replaceFirst("[\\s]{1,}", "").replaceAll("[\t\r\n]{1,}", "");
			else {
				ret=vals.replaceFirst("[\\s]{1,}", "").replaceAll("[\t\r\n]{1,}", "");
				isArray=true;
			}
			vals="";
		}
		String bracket = (isArray) ? "[" : "{";
		String[] fillNames = fillName.split(",");
		if (fillNames.length>1) {
			String[] values=ret.split(",");
			ret="";
			for (int i=0;i<fillNames.length;i++) {
				ret+=fillNames[i]+":"+values[i]+",";
			}
			ret="{"+ret.substring(0,ret.length()-1)+"},";
		}
		else
			ret = ((!isArray) ? fillName + ":" : "") + ((!noBrackets) ? bracket : "") + ret;
		//ret = (!isArray) ? fillName + ":" + ((!noBrackets) ? bracket : "") + ret : ((!noBrackets) ? bracket : "") + ret;
		return (ret.equals("")) ? null : (((noBrackets) ? ret.substring(0,ret.length()-1)  : (ret.substring(0,ret.length()-1) + ((bracket.equals("["))?']':'}'))) + ",");
	} 
	
	public void parseFile(String f) {
		try {
			if (JSONElement==null)
				JSONElement = new ArrayList<JSONOperations.JSONEntity>();
			fis = new FileInputStream(f);
			byte read = (byte) fis.read();
			if ((char)read!='[' && (char)read!='{') {
				System.err.println("To nie jest plik JSON! Kończę pracę");
				return;
			}
			for(;;) {
				if ((int)(read=(byte)fis.read())==-1) 
					break;
				if ((char)read=='{') {
					//Log.show("parseFile " + (char)read);
					JSONElement.addAll(readJSONObject());
				}
				if ((char)read=='[') {
					//Log.show("parseFile " + (char)read);
					JSONElement.addAll(readJSONArray());
				}
			}
 			
//			byte read[] = {'\0', '\0'};
//			for (;;) {
//				
//				if ((read=fis.readNBytes(2)).length==0) {
//					break;
//				}
//				Character
//				System.out.println((char)read);
//			}
			//while((read=fis.readNBytes(2))[1]!=Character.MAX_CODE_POINT) {
				
			//}
			//read[1] = '\0';
			//System.out.println(new String(fis.readAllBytes()));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
} 
