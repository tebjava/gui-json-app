package libs;

public class Log {
	
	static int step = 0;
	
	private Log() {}
	
	public static void show(Object o) {
		show(o, null, null, true, true);
	}
	
	public static void show(Object o, boolean error) {
		show(o, null, null, error, true);
	}
	
	public static void show(String s, boolean showStep) {
		show (s, null, null, true, showStep);
	}
	
	private static void show(Object o, Object hint, String file, boolean error, boolean showStep) {
		//final static int step = 0;
		String l="";
		if (showStep) l+="KROK " + (step++) + " : ";
		if (hint!=null) {
			l+=hint+" : ";
		}
		l+=o;
		if (error)
			System.err.println(l);
		else
			System.out.println(l);
	}
}
